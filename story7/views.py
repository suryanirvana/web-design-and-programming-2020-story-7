from django.shortcuts import render, redirect
from .forms import *
from .models import User

# UNDO SAMPE SONI AJA
def index(request):
    if request.method == "POST":
        form = User_Input(request.POST)
        if (form.is_valid()):
            new_name = form.cleaned_data['name']
            new_status = form.cleaned_data['status']
            response = {'name':new_name, 'status':new_status}
            return render(request, 'confirm.html', response)
    else:
        form = User_Input()
    user = User.objects.all()
    response = {'user': user}
    return render(request, 'index.html', response)

def confirmation(request):
    if request.method == "POST":
        form = User_Input(request.POST)
        if (form.is_valid()):
            new_user = User()
            new_user.name = form.cleaned_data['name']
            new_user.status = form.cleaned_data['status']
            new_user.save()
            return redirect('story7:index')
    else:
        form = User_Input()
    return render(request, 'confirm.html')