from django.urls import path
from .views import *

app_name='story7'
urlpatterns = [
    path('', index, name='index'),
    path('confirmation/', confirmation, name='confirmation'),
]