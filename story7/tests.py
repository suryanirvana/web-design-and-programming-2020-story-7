from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .apps import *
from .views import *
from .models import *
from selenium import webdriver
import time

# Create your tests here.
class StoryUnitTest (TestCase):
    def test_homepage_url_template_and_function(self):
        response = Client().get('')
        found = resolve('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)
    
    def test_confirmation_page_url_template_and_function(self):
        response = Client().get('/confirmation/')
        found = resolve('/confirmation/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirm.html')
        self.assertEqual(found.func, confirmation)

    def test_homepage_contains_content(self):
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Surya Nirvana", response_content)
        self.assertIn("Hi there!", response_content)
        self.assertIn("let's get to know each other", response_content)
        self.assertIn("Responses", response_content)
    
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')
    
    def test_confirmation_page_contains_content(self):
        response = Client().get('/confirmation/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Are you sure you want to submit this?", response_content)
    
    def test_model_can_create_new_user_and_return_attribute(self):
        new_user = User.objects.create(name='Surya', status='Not Good')
        count_all_new_user = User.objects.all().count()
        self.assertEqual(count_all_new_user,1)
        self.assertEqual(str(new_user), new_user.name)

    def test_user_form_validation_for_blank(self):
        new_user = User_Input(data={'user': ''})
        self.assertFalse(new_user.is_valid())
        self.assertEqual(new_user.errors['name'], ["This field is required."])
    
    def test_form_validation_for_filled_items(self) :
        response = self.client.post('/confirmation/', data = {'name':'Surya', 'status':'Not Good'})
        count_all_user = User.objects.all().count()
        self.assertEqual(count_all_user, 1)

        self.assertEqual(response.status_code, 302)

        new_response = self.client.get('/')
        new_response_content = new_response.content.decode('utf-8')
        self.assertIn('Surya', new_response_content)
        self.assertIn('Not Good', new_response_content)
    
    def test_confirmation_page(self) :
        response = Client().post('', {'name': 'Surya', 'status': 'Not Good'})
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'confirm.html')

class StoryFunctionalTest(LiveServerTestCase):
    def setUp(self) :
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()

    def test_functional(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        # Cancel Button is Clicked
        self.driver.find_element_by_name('name').send_keys('Surya')
        self.driver.find_element_by_name('status').send_keys('Not Good')
        self.driver.find_element_by_name('submit').click()

        name = self.driver.find_element_by_name('name').get_attribute('value')
        status = self.driver.find_element_by_name('status').get_attribute('value')
        self.assertIn('Surya', name)
        self.assertIn('Not Good', status)

        self.driver.find_element_by_name('cancel').click()

        self.assertIn('Responses', response_content)

        # Submit Button is Clicked
        self.driver.find_element_by_name('name').send_keys('Surya')
        self.driver.find_element_by_name('status').send_keys('Not Good')
        self.driver.find_element_by_name('submit').click()

        name = self.driver.find_element_by_name('name').get_attribute('value')
        status = self.driver.find_element_by_name('status').get_attribute('value')
        self.assertIn('Surya', name)
        self.assertIn('Not Good', status)

        self.driver.find_element_by_name('confirmation').click()
        
        response_content = self.driver.page_source
        self.assertIn('Surya', response_content)
        self.assertIn('Not Good', response_content)

        # Check the functionality of the color-changer button
        color_before_time = self.driver.find_element_by_id('res').find_element_by_id('time').value_of_css_property('color')
        color_before_name = self.driver.find_element_by_id('res').find_element_by_id('name').value_of_css_property('color')
        color_before_status = self.driver.find_element_by_id('res').find_element_by_id('status').value_of_css_property('color')
        self.assertEqual('rgba(242, 252, 254, 1)', color_before_time)
        self.assertEqual('rgba(242, 252, 254, 1)', color_before_name)
        self.assertEqual('rgba(242, 252, 254, 1)', color_before_status)

        self.driver.find_element_by_class_name('res').find_element_by_id('change-color').click()

        color_after_time = self.driver.find_element_by_id('res').find_element_by_id('time').value_of_css_property('color')
        color_after_name = self.driver.find_element_by_id('res').find_element_by_id('name').value_of_css_property('color')
        color_after_status = self.driver.find_element_by_id('res').find_element_by_id('status').value_of_css_property('color')
        self.assertEqual('rgba(33, 37, 41, 1)', color_after_time)
        self.assertEqual('rgba(33, 37, 41, 1)', color_after_name)
        self.assertEqual('rgba(33, 37, 41, 1)', color_after_status)